import React, { Component } from 'react'

export class Game4 extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
            play : false,
            userGuess : null,
            error : "",
            gameWord = ["python", "reactjs", "javascript", "mongodb", "java", "html", "css", "nodejs"],
            attempt : 0,
            ranWord : "",
            newTempWord : "",
            gameEnd : false,
            score : 0,
        }
    }

    createNewWord = (arr) => {
        for(let i = arr.length - 1 ; i >=0; i--){
            let temp = arr[i]
            let j = Math.floor(Math.random()*(i+1))
            arr[i] = arr[j]
            ar[j] = temp
        }
        return arr
    }

    loadWord = () => {
        let ranWord = this.state.gameWord[this.state.attempt]
        let newTempWord = this.createNewWord(ranWord.split("")).join("")
        this.setState({
            ranWord : ranWord,
            newTempWord : newTempWord
        })
    }

    componentDidMount(){
        this.loadWord()
    }

    clickHandler = () => {
        if(!this.state.play){
            this.setState({
                play : true
            })
        }
    }
    
    nextQuestionHandler = () => {
        if(this.state.userGuess === this.state.ranWord){
            this.loadWord()
            this.setState({
                score : this.state.score + 1,
                userGuess : "",
                error : "",
                attempt : this.state.attempt + 1,
                currentIndex : this.state.currentIndex + 1,
            })
        } else {
            this.loadWord()
            this.setState({
                userGuess : "",
                error : "sorry wrong answer please try again"
            })
        }
    }

    render() {
        return (
            <div>
                <header>
                    <h1>THE SCRAMBLE GAME</h1>
                </header>
                <section>
                    <div className='gameArea'>
                        {this.state.play ?
                            <div>
                                <h3 className='msg'>{`Guess the Word "{this.state.newTempWord}"`}</h3>
                                <input className='text' value={this.state.userGuess} onChange={(e) => this.setState({ userGuess: e.target.value })} /><br />
                                {this.state.error && <p>{this.state.error}</p>}
                                {this.state.attempt < this.state.gameWord.length - 1 ? <button className='btn' onClick={() => this.nextQuestionHandler()}>Next Question</button> : null}
                            </div> :
                            <button className={!this.state.play ? "btn" : "hidden"} onClick={() => this.clickHandler()}>Start the Game</button>
                        }
                    </div>
                </section>
            </div>
        )
    }
}

export default Game4

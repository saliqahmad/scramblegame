import React, { Component } from 'react'
import Game from './Game'
import './Style.css'

export class Game3 extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userGuess: "",
            play: false,
            msg: "",
            btn: "Start the Game",
            gameEnd: false,
            gameWords: ["python", "react", "nodejs", "javascript", "mongodb", "angular", "java", "html"],
            currentIndex: 0,
            score: 0,
            ranWord: "",
            result : [],
            newTempWord: "",
            error: "",
            attempt: 0,
            answer : null
        }
    }

    //createNewWord is for shuffling the letters of a word
    createNewWord = (arr) => {
        for (let i = arr.length - 1; i >= 0; i--) {
            let temp = arr[i];
            let j = Math.floor(Math.random() * (i + 1))
            arr[i] = arr[j]
            arr[j] = temp
        }
        return arr
    }

    loadWord = () => {
        let ranNum = Math.floor(Math.random()*this.state.gameWords.length)
        let ranWord = this.state.gameWords[ranNum]
        let newTempWord = this.createNewWord(ranWord.split("")).join("")
        this.setState({
            ranWord: ranWord,
            newTempWord: newTempWord,
            answer : ranWord    
        })
    }

    componentDidMount() {
        this.loadWord()
    }

    clickHandler = () => {
        if (!this.state.play) {
            this.setState({
                play: true,
            })
        }
    }

    nextGuessHandler = () => {
        if(this.state.userGuess < 1){
            alert("please enter an answer")
        } else{
            let {result} = this.state
            result.push({correctAnswer: this.state.answer, userGuess : this.state.userGuess, answer: this.state.answer === this.state.userGuess? "Correct" : "Incorrect"})
            this.setState({
                result
            })
        }
        
        if(this.state.userGuess === this.state.ranWord){
            this.loadWord()
            this.setState({
                score : this.state.score + 1,  
                userGuess : "",
                answer : this.state.ranWord,
                error : "",
                attempt: this.state.attempt + 1,
                currentIndex : this.state.currentIndex + 1,
            })
        } else {
            this.loadWord()
            this.setState({
                userGuess : "",
                error : `Sorry wrong answer please try again`,
                // attempt: this.state.attempt + 1
            })
        }
    }

    finishHandler = () => {
        if (this.state.currentIndex === this.state.gameWords.length - 1) {
            this.setState({
                quizEnd: true
            })
        }
    }

    render() {
        console.log(this.state.currentIndex)
        if (this.state.quizEnd) {
            return (
                <div>
                    <h1>Game Over</h1>
                    <h3 className="msg">{`Quiz Ends, your score is ${this.state.score}`}</h3>
                    <p>Correct answer are :-</p>
                    <ul>
                        {this.state.result.map((item, index) =>  (
                            <li key={index}>
                                {`Q ${index+1}: Correct answer is ${item.correctAnswer}, Your answer is ${item.userGuess} and It is ${item.answer}`}</li>
                        ))}
                    </ul>
                </div>
            )
        }

        return (
            <div>
                <header>
                    <h1>THE SCRAMBLE GAME</h1>
                </header>
                <section>
                    <div className="gameArea">
                      { this.state.play ?
                       <div>
                        <h3 className='msg'>{`Guess the word "${this.state.newTempWord}"`}</h3>
                          <input className="text" value={this.state.userGuess} onChange={(e) => this.setState({userGuess : e.target.value})} /><br/>
                          {this.state.error && <p>{this.state.error}</p>}
                          { this.state.attempt < this.state.gameWords.length-1 ? <button className='btn' onClick={() => this.nextGuessHandler()}>Next Question</button> : null }
                        </div> : 
                         <button className={!this.state.play ? "btn" : "hidden"} onClick={() => this.clickHandler()}>Start the game</button>
                      } 
                        { this.state.attempt >= this.state.gameWords.length -1 ? <button className='btn' onClick={this.finishHandler}>Finish</button> : null }

                    </div>
                </section>
            </div>
        )
    }
}

export default Game3

import React, { Component } from 'react'
import './Style.css'

class Game extends Component {
    constructor(props) {
        super(props)

        this.state = {
            play : false,
            newWord : "",
            randomWord : "",
            newTempWords : "",
            btn : "play",
            ranNum : "",
            msg : "",
            
            sWord : [
                "python", "react", "javascript", "angular", "swift" , "java", "forton", "php", "html", "css", "mongodb", "nodejs",
            ],
            userInput : "",
        }
    }

    createNewWord= () => {
        
            // let play = true
            let ranNum = Math.floor(Math.random()*this.state.sWord.length)
            let newTempWords = this.state.sWord[ranNum]
            let randomWord = this.scrambleWord(newTempWords.split("")).join("")
            console.log("a",randomWord)
        this.setState({
            // play : true,
             randomWord : randomWord

        })
        
    }

    scrambleWord = (arr) => {
        for(let i = arr.length-1; i >=0; i--){
            let temp = arr[i]
            let j = Math.floor(Math.random()*(i+1))
            arr[i] = arr[j]
            arr[j] = temp
        }
        return arr
    }

    componentDidMount(){
        this.createNewWord()
    }

    clickHandler = () => {
        
        this.setState({
            play : true,
            msg : `Guess the word ${this.randomWord}`,
            
            
        })

        if(this.state.userInput === this.newTempWords){
            this.setState({
                msg : "Congrats",
                btn : "next"
            })
        } else {
            this.setState({
                msg : "Sorry ",
                btn : "Try again"
            })
        }
    }

    
    

    render() {
        
        
        return (

            <div>
                <header>
                    <h1>GUESS WORD GAME</h1>


                </header>
                <section>
                    <div className="gameArea">
                        <h3 className='msg'>{this.state.msg}</h3>
                        <input className= {this.state.play ? 'text' : 'hidden'}  onChange= {(e) => {
                            this.setState({
                                userInput : e.target.value
                            })
                        }}>
                            
                        </input>
                        <button className='btn' onClick = {() => this.clickHandler()}>{this.state.btn}</button>
                    </div>
                </section>

            </div>
        )
    }
}

export default Game

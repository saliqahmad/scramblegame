import React, { Component } from 'react'
import './Style.css'

export class Game2 extends Component {
    constructor(props) {
        super(props)

        this.state = {
            play: false,
            userInput: "",
            score: 0,
            newWord: "",
            randomWord: "",
            newTempWord: "",
            ranNum: "",
            msg: "",
            btn: "play ",
            words: [
                "python", "react", "javascript", "angular", "swift", "java", "forton", "php", "html", "css", "mongodb", "nodejs",
            ],
            currentIndex : 0,
        }
    }

    createNewWord = () => {
        let ranNum = Math.floor(Math.random() * this.state.words.length)
        let newTempWord = this.state.words[ranNum]
        let randomWord = this.scrambleWord(newTempWord.split("")).join("")
        this.setState({
            randomWord: randomWord,
            newTempWord : newTempWord
        })
    }

    scrambleWord = (arr) => {
        for (let i = arr.length - 1; i >= 0; i--) {
            let temp = arr[i]
            let j = Math.floor(Math.random() * (i + 1))
            arr[i] = arr[j]
            arr[j] = temp

        }
        return arr
    }

    componentDidMount() {
        this.createNewWord()
    }

    clickHandler = () => {

        if (!this.state.play) {
            this.setState({
                play: true,
                msg: `Guess the word ${this.state.randomWord}`,
                btn: "Guess"
            })
        } else if (this.state.userInput === this.state.newTempWord) {
            this.setState({
                play: false,
                msg: "Congrats",       
                userInput : " ",
                btn : ""

            })
        } else {
            this.setState({
                msg: "Try again youre incorrect",
                btn: "Try again",

            })
        }

    }

    nextQuestionHandler = () => {
        if(this.state.userInput === this.newTempWord){
            this.setState({
                score : this.state.score + 1,
            })
        } 
        this.setState({
            currentIndex : this.state.currentIndex + 1
        })
    }




    render() {
        return (
            <div>
                <header>
                    <h1>THE SCRAMBLE WORD GAME</h1>
                </header>
                <section>
                    <div className='gameArea'>
                        <h3 className='msg'>{this.state.msg}</h3>
                        <input className={this.state.play ? "text" : "hidden"}
                            onChange={(e) => {
                                this.setState({
                                    userInput: e.target.value
                                })
                            }}
                        ></input>
                        <button className={this.state.play ? 'hidden' : "btn"} onClick={() => this.clickHandler()}>{this.state.btn}</button>
                        <button className = "btn" onClick = {() => this.nextQuestionHandler}>Next Question</button>
                    </div>

                </section>

            </div>
        )
    }
}

export default Game2

import logo from './logo.svg';
import './App.css';
// import Game from './Components/Game';
import Game3 from './Components/Game3';

function App() {
  return (
    <div className="App">
      <Game3 />
    </div>
  );
}

export default App;
